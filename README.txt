Category permissions
--------------------

Goals:
=> Create permissions based on taxonomy entries
=> Provide simple permissions for other modules to restrict access to any content by taxonomy terms.

Installation:
-------------
drush en category_permission
OR
Go to: /admin/modules/ and enable category pemrissions.

Configureation:
---------------
Go to: /admin/config/content/category_permissions and choose your vocabularies.

Example usage:
--------------

if(!user_access('view '.$term->tid.' category')) {
  drupal_set_message(t('AHAH, you have no permission to see this! :P'));
  return FALSE;
}

if(!user_access('use '.$term->tid.' category')) {
  drupal_set_message(t('AHAH, you have no permission to user this category! :P'));
  return FALSE;
}
